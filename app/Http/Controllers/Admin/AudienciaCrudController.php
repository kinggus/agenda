<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AudienciaRequest as StoreRequest;
use App\Http\Requests\AudienciaRequest as UpdateRequest;
use App\User;
use Backpack\CRUD\CrudPanel;

/**
 * Class AudienciaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AudienciaCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Audiencia');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/audiencia');
        $this->crud->setEntityNameStrings('audiencia', 'audiencias');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();


        $this->crud->enableExportButtons();
        $this->crud->orderBy('data', 'asc');
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Buscar Datas'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'data', '>=', $dates->from);
                $this->crud->addClause('where', 'data', '<=', $dates->to);
            }
        );
        $this->crud->addFilter([
            'name'  => 'users',
            'type'  => 'select2_multiple',
            'label' => 'Participantes'
        ], function () { // the options that show up in the select2
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('users', function ($query) use ($value) {
                    $query->where('user_id', $value);
                });
            }
        });
        //$this->crud->setFromDb(); // columns
        $this->crud->addColumn(['name' => 'n', 'label' => 'Nº do processo']);
        $this->crud->addColumn([
            'name'        => 'data',
            'label'       => 'Data',
            'type'        => 'date',

        ]);
        $this->crud->addColumn([
            'name'        => 'hora',
            'label'       => 'Hora',
            'type'        => 'time',

        ]);
        $this->crud->addColumn([
            'name'        => 'tipo',
            'label'       => 'Tipo',
            'type'        => 'radio',
            'options'     => [
                0 => 'Presencial',
                1 => 'Online'
            ]

        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Participantes', // Table column heading
            'type'      => 'select_multiple',
            'name'      => 'users', // the method that defines the relationship in your Model
            'entity'    => 'users', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => 'App\User', // foreign key model
        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Descrição', // Table column heading

            'name'      => 'descricao', // the method that defines the relationship in your Model

        ]);


        ////// create

        $this->crud->addField([   // radio
            'name'        => 'n', // the name of the db column
            'label'       => 'Nº do Processo', // the input label


            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);
        $this->crud->addField([   // radio
            'name'        => 'data', // the name of the db column
            'label'       => 'Data', // the input label
            'type'        => 'date',

            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);
        $this->crud->addField([   // radio
            'name'        => 'hora', // the name of the db column
            'label'       => 'Hora', // the input label
            'type'        => 'time',

            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Representante (Interno)",
            'type'      => 'select2_multiple',
            'name'      => 'users', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'users', // the method that defines the relationship in your Model
            'model'     => "App\User", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);


        $this->crud->addField([   // radio
            'name'        => 'tipo', // the name of the db column
            'label'       => 'Tipo', // the input label
            'type'        => 'radio',
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                0 => "Presencial",
                1 => "Online"
            ],
            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);

        $this->crud->addField([   // radio
            'name'        => 'descricao', // the name of the db column
            'label'       => 'Descrição', // the input label
            'type'        => 'simplemde',

            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);

        // add asterisk for fields that are required in AudienciaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
