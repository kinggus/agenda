<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AgendaRequest as StoreRequest;
use App\Http\Requests\AgendaRequest as UpdateRequest;
use App\User;
use Backpack\CRUD\CrudPanel;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

/**
 * Class AgendaCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AgendaCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Agenda');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/agenda');
        $this->crud->setEntityNameStrings('agenda', 'agendas');
        $user = backpack_auth()->user()->id;
        //dd($user);
        if (backpack_user()->hasRole('comum')) {
            $this->crud->query = $this->crud->query->whereHas('users', function ($query) use ($user) {
                $query->where('user_id', $user);
            });
        }

        $this->crud->enableExportButtons();
        $this->crud->allowAccess('show');
        $this->crud->orderBy('data', 'asc');
        $this->crud->orderBy('hora', 'asc');
        $now = date('Y-m-d');
        //  dd($now);
        $this->crud->addClause('where', 'data', '>=', $now);

        $this->crud->addFilter([
            'name'  => 'users',
            'type'  => 'select2_multiple',
            'label' => 'Participantes'
        ], function () { // the options that show up in the select2
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('users', function ($query) use ($value) {
                    $query->where('user_id', $value);
                });
            }
        });
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Buscar Datas'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'data', '>=', $dates->from);
                $this->crud->addClause('where', 'data', '<=', $dates->to);
            }
        );
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();
        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Participantes",
            'type'      => 'select2_multiple',
            'name'      => 'users', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'users', // the method that defines the relationship in your Model
            'model'     => "App\User", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);


        $this->crud->addField([   // radio
            'name'        => 'tipo', // the name of the db column
            'label'       => 'Tipo', // the input label
            'type'        => 'radio',
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                0 => "Presencial",
                1 => "Online"
            ],
            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);

        $this->crud->addField([   // radio
            'name'        => 'descricao', // the name of the db column
            'label'       => 'Descrição', // the input label
            'type'        => 'simplemde',

            // optional
            //'inline'      => false, // show the radios all on the same line?
        ]);

        $this->crud->addField([   // Summernote
            'name'  => 'descricao',
            'label' => 'Descrição',
            'type'  => 'summernote',
            'options' => []
        ]);

        // the summernote field works with the default configuration options but allow developer to configure to his needs
        // optional configuration check https://summernote.org/deep-dive/ for a list of available confi);



        $this->crud->addColumn([
            'name'        => 'data',
            'label'       => 'Data',
            'type'        => 'date',

        ]);
        $this->crud->addColumn([
            'name'        => 'hora',
            'label'       => 'Hora',
            'type'        => 'time',

        ]);
        $this->crud->addColumn([
            'name'        => 'tipo',
            'label'       => 'Tipo',
            'type'        => 'radio',
            'options'     => [
                0 => 'Presencial',
                1 => 'Online'
            ]

        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Participantes', // Table column heading
            'type'      => 'select_multiple',
            'name'      => 'users', // the method that defines the relationship in your Model
            'entity'    => 'users', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => 'App\Models\User', // foreign key model
        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Descrição', // Table column heading
            'type' => 'textarea',
            'name'      => 'descricao', // the method that defines the relationship in your Model
            'limit' =>  450

        ]);
        // add asterisk for fields that are required in AgendaRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);

/*

        $url = "https://messages-sandbox.nexmo.com/v0.1/messages";
        $params = [
            "to" => ["type" => "whatsapp", "number" => "559681273875"],
            "from" => ["type" => "whatsapp", "number" => "14157386170"],
            "message" => [
                "content" => [
                    "type" => "text",
                    "text" => $this->entry('data') . "Hello from Vonage and Laravel :) Please reply to this message with a number between 1 and 100"
                ]
            ]
        ];
        $headers = ["Authorization" => "Basic " . base64_encode('2c7cf11a' . ":" . 'X2Ppmqiogg2xcOQc')];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, ["headers" => $headers, "json" => $params]);
        $data = $response->getBody();
        Log::Info($data);

        */
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        //dd($this->data['entry']->data);
        //$novo = Carbon::createFromFormat('Y-m-d', $this->data['entry']->data)->toDateTimeString();

        /*
        $novo = \Carbon\Carbon::parse($this->data['entry']->data)
            ->locale(App::getLocale())
            ->isoFormat($column['format'] ?? config('backpack.base.default_date_format'));
       // dd($novo);
        $url = "https://messages-sandbox.nexmo.com/v0.1/messages";
        $params = [
            "to" => ["type" => "whatsapp", "number" => "559681273875"],
            "from" => ["type" => "whatsapp", "number" => "14157386170"],
            "message" => [
                "content" => [
                    "type" => "text",
                    "text" => $novo . "\n".$this->data['entry']->hora."\n".
                    strip_tags($this->data['entry']->descricao)
                ]
            ]
        ];
        $headers = ["Authorization" => "Basic " . base64_encode('2c7cf11a' . ":" . 'X2Ppmqiogg2xcOQc')];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, ["headers" => $headers, "json" => $params]);
        $data = $response->getBody();
        Log::Info($data);

        */

        return $redirect_location;
    }
    protected function setupShowOperation()
    {
        // by default the Show operation will try to show all columns in the db table,
        // but we can easily take over, and have full control of what columns are shown,
        // by changing this config for the Show operation
        $this->crud->set('show.setFromDb', false);

        // example logic
        $this->crud->addColumn([
            'name'        => 'data',
            'label'       => 'Data',
            'type'        => 'date',

        ]);
        $this->crud->addColumn([
            'name'        => 'hora',
            'label'       => 'Hora',
            'type'        => 'time',

        ]);
        $this->crud->addColumn([
            'name'        => 'tipo',
            'label'       => 'Tipo',
            'type'        => 'radio',
            'options'     => [
                0 => 'Presencial',
                1 => 'Online'
            ]

        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Participantes', // Table column heading
            'type'      => 'select_multiple',
            'name'      => 'users', // the method that defines the relationship in your Model
            'entity'    => 'users', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => 'App\User', // foreign key model
        ]);
        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label'     => 'Descrição', // Table column heading

            'name'      => 'descricao', // the method that defines the relationship in your Model

        ]);
        // $this->crud->removeColumn('date');
        // $this->crud->removeColumn('extras');

        // Note: if you HAVEN'T set show.setFromDb to false, the removeColumn() calls won't work
        // because setFromDb() is called AFTER setupShowOperation(); we know this is not intuitive at all
        // and we plan to change behaviour in the next version; see this Github issue for more details
        // https://github.com/Laravel-Backpack/CRUD/issues/3108
    }
}
