<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i>
        <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('agenda') }}'><i class='fa fa-calendar'></i> <span>Agendas</span></a></li>
<li><a href='{{ backpack_url('audiencia') }}'><i class='fa fa-calendar'></i> <span>Audiências</span></a></li>
@if (backpack_user()->hasRole('admin'))
    <li class="treeview">
        <a href="#"><i class="fa fa-users"></i><span> Administração</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span></a>
        <ul class="treeview-menu">
            <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Usuários</span></a></li>
            <li><a href="{{ backpack_url('role') }}"><i class="fa fa-times"></i> <span>Regra</span></a></li>
            <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissão</span></a></li>
        </ul>
    </li>

@endif
